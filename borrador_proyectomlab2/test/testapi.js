var mocha = require('mocha');
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server_v3');
var should = chai.should();

chai.use(chaiHttp);

describe('Pruebas Colombia', () => {
  it('should ', (done) => {
      chai.request('http://www.bbva.com')
        //console.log("inicia prueba")
        .get('/')
        .end((err, res)=>{
          console.log("esta es la respuesta");
          //console.log(res)
          res.should.have.status(200)
          done()
        })
  });
  it('should2 ', (done) => {
      chai.request('http://www.fifa.com')
        //console.log("inicia prueba")
        .get('/')
        .end((err, res)=>{
          console.log("esta es la respuesta");
          //console.log(res)
          res.should.have.status(200)
          done()
        })
  });
  it('Mi api funciona', (done) => {
    chai.request('http://localhost:3002')
      .get('/colapi/v3/users')
      .end((err,res) =>{
        console.log(res.body)
        //res.body.should.be.a('array')
        //res.body.length.should.be.gte(1)
       done();
    })
  });


it('devuelve un array', (done) => {
  chai.request('http://localhost:3002')
    .get('/colapi/v3/users')
    .end((err,res) =>{
      console.log(res.body)
      res.body.should.be.a('array')

     done();
  })
});

it('devuelve al menos un elemento', (done) => {
  chai.request('http://localhost:3002')
    .get('/colapi/v3/users')
    .end((err,res) =>{
      console.log(res.body)
      //res.body.should.be.a('array')
      res.body.length.should.be.gte(1)
     done();
  })
});

it('validar primer elemento ', (done) => {
  chai.request('http://localhost:3002')
    .get('/colapi/v3/users')
    .end((err,res) =>{
      console.log(res.body[0])
      //res.body.should.be.a('array')
      res.body[0].should.have.property('first_name')
      res.body[0].should.have.property('last_name')
     done();
  })
});
it('POST validar CREAR  elemento ', (done) => {
  chai.request('http://localhost:3002')
    .post('/colapi/v3/users')
    .send('{"id":4813,"first_name":"fulanito","last_nme":"ootro","email": "troppa@kickstarter.com","password": "PjRG1oRy"}')
    .end((err,res,body) =>{
      //console.log(res.body[0])
      //res.body.should.be.a('array')
      console.log(body);
     done();
  })
});
});
