var express = require('express');
var bodyParser = require('body-parser');
var requestJSON = require('request-json');
var app = express();
var port = process.env.PORT || 3002;
var usersFile = require('./users.json');
var URLbase = "/colapi/v3/";
var basemlabURL = "https://api.mlab.com/api/1/databases/colapidb_jdn/collections/";
var apikeymlab = "apiKey=0RarnOZbIjSNI2c1jDkxgt7ffZFEAmIy";

app.listen(port, function(){
 console.log("API apicol escuchando en el puerto " + port + "...");
});

app.use(bodyParser.json());


// GET users -- retorna todods los usuarios de mlab
app.get(URLbase + 'users',
 function(req, res) {
   console.log("GET /apicol/v3/users");
   var filtro = 'f={"_id": 0}&';
   var httpClient = requestJSON.createClient(basemlabURL);
   console.log("cliente http de mlab creado");
   httpClient.get ("user?"+ filtro + apikeymlab,
    function (err, respuestaMlab,body){
      console.log("Error " + err);
      console.log("resmlab " + respuestaMlab);
      //console.log(body);
      //var respuesta = body;
      var respuesta = {};
      respuesta = !err ? body : {"msg":"error al recuperar users de mlab"};
      res.send(respuesta);
    });
  });
//*******
// Petición GET con con usuario id en mlab
app.get(URLbase + 'users/:id',
 function (req, res) {
   var filtro = 'f={"_id": 0}&';
   console.log("GET /colapi/v3/users/:id");
   //console.log(req.params.id);
   console.log('req.params.id: ' + req.params.id);
   //console.log('req.params.otro: ' + req.params.otro);
   var id = req.params.id;
   var respuesta = req.params;
   var queryString = 'q={"id":'+ id + '}&';
   console.log("qeury " + queryString);
   var httpClientx = requestJSON.createClient(basemlabURL);
   httpClientx.get ('user?'  + filtro + queryString + apikeymlab,
    function(err, respuestaMlab, bodyx)
     {
       console.log("error = "+ err);
       console.log("respuesta mlab correcta");
       var respuesta = bodyx;
       res.send(respuesta);
     });
});

//peticion get para todas las cuentas
app.get(URLbase + 'account',
 function(req, res) {
   console.log("GET /apicol/v3/account");
   var filtro = 'f={"_id": 0}&';
   var httpClient = requestJSON.createClient(basemlabURL);
   console.log("cliente http de mlab creado");
   httpClient.get ("account?"+ filtro + apikeymlab,
    function (err, respuestaMlab,body){
      console.log("Error " + err);
      console.log("resmlab " + respuestaMlab);
      console.log("bodiy " + body);
      var respuesta = {};
      respuesta = !err ? body : {"msg":"error al recuperar Account de mlab"};
      res.send(respuesta);
    });
  });
//*******
// Petición GET a account con con user_id en mlab
app.get(URLbase + 'users/:user_id/account/',
 function (req, res) {
   var filtro = 'f={"_id": 0}&';
   console.log("GET /colapi/v3/user/:user_id/account");
   console.log('req.params.user_id: ' + req.params.user_id);
   var id = req.params.user_id;
   var respuesta = req.params;
   var queryString = 'q={"user_id":'+ id + '}&';
   var httpClient = requestJSON.createClient(basemlabURL);
   httpClient.get ('account?' + filtro + queryString + apikeymlab,
    function(err, respuestaMlab, body)
     {
       console.log("respuesta mlab correcta");
       var respuesta = body;
       res.send(respuesta);
    });
});


// Petición GET a movientos  en mlab
app.get(URLbase + 'users/account/:iban/movement/',
 function (req, res) {
   var filtro = 'f={"_id": 0}&';
   console.log("GET /colapi/v3/users/account/:iban/movement/");
   console.log('req.params.IBAN: ' + req.params.iban);
   var ib = req.params.iban;
   var respuesta = req.params;
   var queryString = 'q={"iban":"'+ ib + '"}&';
   var httpClient = requestJSON.createClient(basemlabURL);
   httpClient.get ('movement?' + filtro + queryString + apikeymlab,
    function(err, respuestaMlab, body)
     {
       console.log("respuesta mlab correcta");
       console.log("body" + body);
       var respuesta = body;
       res.send(respuesta);
    });
});

//post con mlab

app.post(URLbase + 'users', function(req, res) {
console.log("req body"+ req.body);
 clienteMlab = requestJSON.createClient(basemlabURL + "/user?" + apikeymlab)
 clienteMlab.post('', req.body, function(err, resM, body) {
   console.log("err = " + err);
   res.send(body);
 });
});
//final post con mlab

// put con mlab
app.put(URLbase + 'users/:id', function(req, res) {
 clienteMlab = requestJSON.createClient(basemlabURL + "/user?")
 var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
 clienteMlab.put('?q={"id": ' + req.params.id + '}&' + apikeymlab, JSON.parse(cambio), function(err, resM, body) {
   console.log("error = " + err);
   console.log("resM = " + resM);
   res.send(body);
 });
});
// final pub con mlab

  //peticion post para login con mlab
  app.post(URLbase + 'login',
   function(req, res) {
     console.log("login /colapi/v3....");
     console.log(req.body.email);
     console.log(req.body.password);
     var resemail = "";
     var pass = req.body.password;
     var respass = req.body.password;
     var email = req.body.email;
     var filtro = 'f={"_id": 0}&';
     var id = "";
     var fn = "";
     var ln="";
     var queryString = 'q={"email":"' + email +'"}&';
     var httpClient = requestJSON.createClient(basemlabURL);
     //var body = JSON.stringify(req.body);
     //bodyx.push({logged:true});
     console.log("body agregado - req body.email =  " + req.body.email);
     httpClient.get ('user?' +filtro  + queryString + apikeymlab,
     function(err, respuestaMlab, body)
     {
      console.log("error = "+ err);
      console.log("query = "+ queryString);
      console.log("body = " + body[0]);
      console.log("res mlab = " + respuestaMlab);
      if(Object.keys(body).length > 0 ){
       resemail = body[0].email;
       respass = body[0].password;
       id = body[0].id;
       fn= body[0].first_name;
       ln= body[0].last_name;
       console.log("pass de body : "+ body[0].password);
       console.log("email de body : "+ body[0].email);
       console.log(" fn = " + fn);
       console.log(" ln = " + ln);
     }
       if(resemail == email){
         if(respass == pass){
           clienteMlab = requestJSON.createClient(basemlabURL + "/user?")
           var cambio = '{"$set":' + JSON.stringify({"logged":true}) + '}';
           clienteMlab.put('?q={"id": ' + id + '}&' + apikeymlab, JSON.parse(cambio), function(err, resM, body) {
             console.log("error = " + err);
             console.log("resM = " + resM);
             res.send({"msg":"Usuario loggado ok -2","id":id,"fn":fn,"ln":ln});
           });
         }else{
             res.send({"msg":"error password  loggin"});
         }
       }else{
         res.send({"msg":"error email no encontrado -2"});
       }
     });
  });

  //peticion logout
  //peticion post para logout - mlab
  app.post(URLbase + 'logout',
   function(req, res) {
     console.log("login /colapi/v3....");
     console.log(req.body.email);
     console.log(req.body.password);
     var resemail = "";
     var pass = req.body.password;
     var respass = req.body.password;
     var email = req.body.email;
     var filtro = 'f={"_id": 0}&';
     var id = "";
     var queryString = 'q={"email":"' + email +'"}&';
     var httpClient = requestJSON.createClient(basemlabURL);
     var body = JSON.stringify(req.body);
     httpClient.get ('user?' +filtro  + queryString + apikeymlab,
     function(err, respuestaMlab, body)
     {
      console.log("error = "+ err);
      console.log("query = "+ queryString);
      if(Object.keys(body).length > 0 ){
        resemail = body[0].email;
        console.log("body = " + body);
        respass = body[0].password;
        id = body[0].id;
        }
       console.log("pass de body : "+ respass);
       if(resemail == email){
           clienteMlab = requestJSON.createClient(basemlabURL + "/user?")
           var cambio = '{"$unset":' + JSON.stringify({"logged":true}) + '}'
           clienteMlab.put('?q={"id": ' + id + '}&' + apikeymlab, JSON.parse(cambio), function(err, resM, body) {
             console.log("error = " + err);
             console.log("resM = " + resM);
             res.send({"msg":"Usuario DES-loggado ok"});
           });
       }else{
         res.send({"msg":"error email no encontrado-2"});
       }
     });
  });

  function writeUserDataToFile(data) {
   var fs = require('fs');
   var jsonUserData = JSON.stringify(data);
   fs.writeFile("./users.json", jsonUserData, "utf8",
    function(err) { //función manejadora para gestionar errores de escritura
      if(err) {
        console.log(err);
      } else {
        console.log("Datos escritos en 'users.json'.");
      }
    })
  }
